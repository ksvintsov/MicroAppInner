package com.svintsov.inner.repository;

import com.svintsov.inner.entity.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository<Account,String> {
}
