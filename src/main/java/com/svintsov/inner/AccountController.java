package com.svintsov.inner;

import com.svintsov.inner.entity.Account;
import com.svintsov.inner.exception.AccountNotFoundException;
import com.svintsov.inner.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @GetMapping("/{id}")
    public Account getAccount(@PathVariable("id") String id) {

        Optional<Account> account = accountRepository.findById(id);

        if (account.isPresent()){
            return account.get();
        }

        throw new AccountNotFoundException(id);
    }

    @PostMapping(value = "/account", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createAccount(@RequestBody Account account) {

        accountRepository.save(account);

    }

    @GetMapping("/all")
    public List<Account> getAllAccounts() {

        return accountRepository.findAll();

    }

    @DeleteMapping("/{id}")
    public void deleteAccount(@PathVariable("id") String id) {

        accountRepository.deleteById(id);

    }


}
